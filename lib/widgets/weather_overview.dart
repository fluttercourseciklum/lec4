import 'package:flutter/material.dart';
import 'package:lec4/models/weather.dart';
import 'package:lec4/providers/weather_provider.dart';
import 'package:lec4/widgets/weather_container.dart';

class WeatherOverview extends StatelessWidget {
  final _weatherProvider = WeatherProvider();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Weather Overview'),
        ),
        body: OrientationBuilder(builder: (context, orientation) {
          if (orientation == Orientation.portrait) {
            return Center(
                child: FutureBuilder<Weather>(
                    future: _weatherProvider.getCurrentWeather(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return WeatherContainer(weather: snapshot.data);
                      } else if (snapshot.hasError) {
                        return Text('${snapshot.error}');
                      }
                      return const CircularProgressIndicator();
                    }));
          }

          return FutureBuilder<Weather>(
              future: _weatherProvider.getCurrentWeather(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return WeatherContainer(weather: snapshot.data);
                } else if (snapshot.hasError) {
                  return Text('${snapshot.error}');
                }
                return const CircularProgressIndicator();
              });
        }));
  }
}
